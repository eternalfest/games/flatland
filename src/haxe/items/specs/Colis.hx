package items.specs;

import patchman.IPatch;
import etwin.ds.FrozenArray;
import patchman.Ref;
import vault.ISpec;

@:build(patchman.Build.di())
class Colis implements ISpec {
    public var id(default, null): Int = 101;

    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public function new() {
        var patches = [
            Ref.auto(hf.entity.bomb.bad.PoireBomb.onExplode).before(function(hf: hf.Hf, self: hf.entity.bomb.bad.PoireBomb): Void {
                var bosses = self.game.getClose(hf.Data.BOSS, self.x, self.y, self.radius, false);
                for (i in 0...bosses.length) {
                    (cast bosses[i]: hf.entity.boss.Bat).freeze(hf.Data.FREEZE_DURATION);
                }
            }),
        ];
        this.patches = FrozenArray.from(patches);
    }

    public function execute(hf: hf.Hf, specMan: hf.SpecialManager, item: hf.entity.Item): Void {
        hf.entity.bomb.bad.PoireBomb.attach(specMan.game, item.x, item.y).moveUp(10);
    }

    public function interrupt(hf: hf.Hf, specMan: hf.SpecialManager): Void {
    }
}
