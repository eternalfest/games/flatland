package items.specs;

import merlin.value.MerlinFloat;
import etwin.Obfu;
import merlin.Merlin;
import patchman.IPatch;
import etwin.ds.FrozenArray;
import patchman.Ref;
import vault.ISpec;

@:build(patchman.Build.di())
class Letters implements ISpec {
    public var id(default, null): Int = 0;

    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public function new() {}

    public function execute(hf: hf.Hf, specMan: hf.SpecialManager, item: hf.entity.Item): Void {
        if (specMan.actives[27]) {
            specMan.player.getScore(item, 25);
        }
        specMan.player.getScoreHidden(5);
        specMan.player.getExtend(item.subId);
        Merlin.setLevelVar(specMan.game.world.current, Obfu.raw("LETTER_STATE"), new MerlinFloat(item.subId));
    }

    public function interrupt(hf: hf.Hf, specMan: hf.SpecialManager): Void {
    }
}
