package items.skins;

import vault.ISkin;

class Ether implements ISkin {
    public static var ID_OBJETINTERDIT: Int = 187;

    public var id(default, null): Int = 1241;
    public var sprite(default, null): Null<String> = null;

    public function new() {}

    public function skin(mc: etwin.flash.MovieClip, subId: Null<Int>): Void {
        mc.gotoAndStop("" + (ID_OBJETINTERDIT + 1));
    }
}
