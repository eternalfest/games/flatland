package items.skins;

import patchman.DebugConsole;
import merlin.value.MerlinFloat;
import etwin.Obfu;
import merlin.Merlin;
import vault.IItem;

class Somnifere implements IItem {
    public static var ID_ANNEAU: Int = 185;

    public var id(default, null): Int = 1239;
    public var sprite(default, null): Null<String> = null;

    public function new() {}

    public function skin(mc: etwin.flash.MovieClip, subId: Null<Int>): Void {
        mc.gotoAndStop("" + (ID_ANNEAU + 1));
    }

    public function execute(hf: hf.Hf, specMan: hf.SpecialManager, item: hf.entity.Item): Void {
        Merlin.setLevelVar(specMan.game.world.current, Obfu.raw("SOMNI_STATE"), new MerlinFloat(item.subId));
    }

    public function interrupt(hf: hf.Hf, specMan: hf.SpecialManager): Void {
    }
}
