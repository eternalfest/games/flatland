package items.skins;

import vault.ISkin;

class Canne implements ISkin {
    public static var ID_CANNE: Int = 2;

    public var id(default, null): Int = 1240;
    public var sprite(default, null): Null<String> = null;

    public function new() {}

    public function skin(mc: etwin.flash.MovieClip, subId: Null<Int>): Void {
        mc.gotoAndStop("" + (ID_CANNE + 1));
    }
}
