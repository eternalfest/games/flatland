package items;

import etwin.ds.FrozenArray;
import patchman.IPatch;
import vault.IItem;
import vault.ISpec;
import vault.ISkin;
import custom_clips.CustomClips;

import items.specs.Colis;
import items.specs.Letters;
import items.skins.Somnifere;
import items.skins.Canne;
import items.skins.Ether;

@:build(patchman.Build.di())
class Items {
    @:diExport
    public var items(default, null): FrozenArray<IItem>;
    @:diExport
    public var specs(default, null): FrozenArray<ISpec>;
    @:diExport
    public var skins(default, null): FrozenArray<ISkin>;

    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public function new(colis: Colis,
                        clips: CustomClips,
                        patches: Array<IPatch>) {
        this.patches = FrozenArray.from(patches);

        this.items = FrozenArray.of((new Somnifere(): IItem));

        this.specs = FrozenArray.of(
            new Colis(),
            new Letters()
        );

        this.skins = FrozenArray.of(
            new Canne(),
            new Ether()
        );
    }
}
