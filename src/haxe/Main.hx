import mc2.Mc2;
import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import game_params.GameParams;
import hide_fields.HideFields;
import vault.Vault;
import items.Items;
import patchman.IPatch;
import patchman.Patchman;
import better_script.NoNextLevel;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    bugfix: Bugfix,
    debug: Debug,
	gameParams: GameParams,
    hideFields: HideFields,
    items: Items,
    vault: Vault,
    mc2: Mc2,
	noNextLevel: NoNextLevel,
	merlin: Merlin,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
