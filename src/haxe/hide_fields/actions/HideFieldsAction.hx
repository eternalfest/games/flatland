package hide_fields.actions;

import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class HideFieldsAction implements IAction {
    public var name(default, null): String = Obfu.raw("hideFields");
    public var isVerbose(default, null): Bool = false;

    private var mod: HideFields;

    public function new(mod: HideFields) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var n = ctx.getBool(Obfu.raw("n"));

        this.mod.hideFields(game, n);

        return false;
    }
}
