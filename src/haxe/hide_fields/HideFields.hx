package hide_fields;
import hide_fields.actions.HideFieldsAction;

import etwin.ds.FrozenArray;
import patchman.IPatch;
import patchman.Ref;
import merlin.IAction;

@:build(patchman.Build.di())
class HideFields {
    @:diExport
    public var hideFieldsAction(default, null): IAction;
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    private var FIELD_LIST = new Array();
    private var TP_LIST = new Array();

    public function new() {
        this.hideFieldsAction = new HideFieldsAction(this);

        var patches = [
            Ref.auto(hf.DepthManager.attach).wrap(function(hf: hf.Hf, self: hf.DepthManager, inst: String, plan: Int, old): etwin.flash.MovieClip {
                var sprite = old(self, inst, plan);
                if (inst == 'field') {
                    this.FIELD_LIST.push(sprite);
                }
                else if (inst == 'hammer_pod'){
                    this.TP_LIST.push(sprite);
                }
                return sprite;
            }),

            Ref.auto(hf.levels.ViewManager.onDataReady).before(function(hf: hf.Hf, self: hf.levels.ViewManager) {
                this.FIELD_LIST = new Array();
                this.TP_LIST = new Array();
            })
        ];
        this.patches = FrozenArray.from(patches);
    }

    public function hideFields(game: hf.mode.GameMode, hide: Bool) {
        for (i in 0...this.FIELD_LIST.length) {
            this.FIELD_LIST[i]._visible = !hide;
        }
        for (i in 0...this.TP_LIST.length) {
            this.TP_LIST[i]._visible = !hide;
        }
    }
}

